package com.gitlab.soshibby.issuereporter.messageappenders.stringjoiner;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.messageappender.MessageAppender;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginDefinition;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginResolver;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StringJoinerAppender implements MessageAppender {

    private PluginResolver pluginResolver;
    private Config configuration;
    private List<MessageAppender> childMessageAppenders;

    public StringJoinerAppender(PluginResolver pluginResolver) {
        this.pluginResolver = pluginResolver;
    }

    @Override
    public void init(String config) {
        configuration = Config.from(config);
        validateConfig(configuration);

        childMessageAppenders = getChildMessageAppenders(configuration.getAppenders());
    }

    @Override
    public String createMessageFrom(LogStatement logTrigger, List<LogStatement> logStatements) {
        return childMessageAppenders.stream()
                .map(messageAppender -> messageAppender.createMessageFrom(logTrigger, logStatements))
                .collect(Collectors.joining(configuration.getDelimiter()));
    }

    private List<MessageAppender> getChildMessageAppenders(List<PluginDefinition> plugins) {
        return pluginResolver.resolve(plugins, MessageAppender.class);
    }

    private void validateConfig(Config config) {
        Assert.notNull(config, "Configuration is null.");
        Assert.hasText(config.getDelimiter(), "Delimiter is null or empty in config.");
        Assert.notEmpty(config.getAppenders(), "Appenders is null or empty in config.");
    }

}

