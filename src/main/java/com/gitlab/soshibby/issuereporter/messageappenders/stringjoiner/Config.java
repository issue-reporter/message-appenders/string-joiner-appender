package com.gitlab.soshibby.issuereporter.messageappenders.stringjoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginDefinition;

import java.io.IOException;
import java.util.List;

@JsonIgnoreProperties("class")
public class Config {
    private static ObjectMapper mapper = new ObjectMapper();
    private String delimiter;
    private List<PluginDefinition> appenders;

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public List<PluginDefinition> getAppenders() {
        return appenders;
    }

    public void setAppenders(List<PluginDefinition> appenders) {
        this.appenders = appenders;
    }

    public static Config from(String config) {
        try {
            return mapper.readValue(config, Config.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
